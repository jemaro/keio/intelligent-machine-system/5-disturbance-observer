Andreu Gimenez Bolinches ([andreu@keio.jp](mailto:andreu@keio.jp))

![assignment](images/assignment.jpg)



The problem has been solved using an interactive Python notebook, which has
been then converted into `pdf` as a report. One can find the source notebook in
the [code
repository](https://gitlab.com/jemaro/keio/intelligent-machine-system/5-disturbance-observer).
No numerical solution has been derived or simulated, the code is meant to
perform analytical calculus in the same way that one could do do by hand.

Initially we will import some general libraries and define an utility printing
function to show the results nicely.


```python
import numpy as np
import control as ct
from sympy import *
from IPython.display import display, Math

init_printing()


# Printing function
def disp(*expresions: list):
    string = ''
    for expr in expresions:
        if isinstance(expr, str):
            string += expr
        elif isinstance(expr, np.ndarray):
            string += latex(Matrix(expr))
        else:
            string += latex(expr)
    display(Math(string))

```

# Disturbance Observer Derived Analytically

We will consider the system given by the following state space equation:

```math
\dot{x} = Ax + Bu
```

```math
x = 
\left[\begin{matrix}\theta\\\dot{\theta}\\ T_{dis}\end{matrix}\right]

```

```math
\frac{d}{dt}
\left[\begin{matrix}\theta\\\dot{\theta}\\ T_{dis}\end{matrix}\right] = 
\left[\begin{matrix}0 & 1 & 0\\0 & 0 & -1/J_{mn}\\ 0 & 0 & 0\end{matrix}\right]
\left[\begin{matrix}\theta\\\dot{\theta}\\ T_{dis}\end{matrix}\right] + 
\left[\begin{matrix}0\\\frac{K_{tn}}{J_{mn}}\\0\end{matrix}\right] I_a^{ref}

```

```math
y = Cx
```

```math
y = \theta = 
\left[\begin{matrix}1 & 0 & 0\\\end{matrix}\right]
\left[\begin{matrix}\theta\\\dot{\theta}\\ T_{dis}\end{matrix}\right] 

```


```python
Jmn, Ktn = symbols('J_{mn}, K_{tn}')
A = Matrix([
    [0, 1, 0],
    [0, 0, -1 / Jmn],
    [0, 0, 0],
    ])
B = Matrix([
    [0],
    [Ktn / Jmn],
    [0],
    ])
C = np.array([[1, 0, 0]])  # Output matrix
D = np.zeros(shape=(C.shape[0], B.shape[1]))
```

## Minimal order observer

```math
\dot{z} = \hat{A}x + ky + MBu
```
```math
\hat{x} = Dz + Hy
```

In order to find a minimal order observer we will now follow the steps
outlined in lecture 8-6 and 8-7.

### Step 1
```math
S = \left[\begin{matrix}C\\W\end{matrix}\right]
```
```math
SAS^{-1} = \left[\begin{matrix}A_{11}&A_{12}\\A_{21}&A_{22}\end{matrix}\right]
```
```math
SB = \left[\begin{matrix}B_1\\B_2\end{matrix}\right]
```


```python
r, n = C.shape

W = np.array([[0, 1, 0], [0, 0, 1]])
S = np.vstack([C, W])
S_1 = np.linalg.inv(S)
disp('S=', S)
SAS = S @ A @ S_1
disp('SAS^{-1}=', SAS)
(A11, A12), (A21, A22) = (np.hsplit(a, [r]) for a in np.vsplit(SAS, [r]))
disp(
    'A_{11}=', A11, '\quad A_{12}=', A12, '\quad A_{21}=', A21,
    '\quad A_{22}=', A22
    )
SB = S @ B
disp('SB=', SB)
B1, B2 = np.vsplit(SB, [r])
disp('B_1=', B1, '\quad B_2=', B2)
```


$`\displaystyle S=\left[\begin{matrix}1 & 0 & 0\\0 & 1 & 0\\0 & 0 & 1\end{matrix}\right]`$



$`\displaystyle SAS^{-1}=\left[\begin{matrix}0 & 1.0 & 0\\0 & 0 & - \frac{1.0}{J_{mn}}\\0 & 0 & 0\end{matrix}\right]`$



$`\displaystyle A_{11}=\left[\begin{matrix}0\end{matrix}\right]\quad A_{12}=\left[\begin{matrix}1.0 & 0\end{matrix}\right]\quad A_{21}=\left[\begin{matrix}0\\0\end{matrix}\right]\quad A_{22}=\left[\begin{matrix}0 & - \frac{1.0}{J_{mn}}\\0 & 0\end{matrix}\right]`$



$`\displaystyle SB=\left[\begin{matrix}0\\\frac{K_{tn}}{J_{mn}}\\0\end{matrix}\right]`$



$`\displaystyle B_1=\left[\begin{matrix}0\end{matrix}\right]\quad B_2=\left[\begin{matrix}\frac{K_{tn}}{J_{mn}}\\0\end{matrix}\right]`$


### Step 2

We define $`\hat{A}`$ taking into account the desired poles of the observer.
Which we will consider to be $`[-g, -g]`$. An appropiate $`L`$ matrix has been
choosen manually in order to place the desired poles.

```math
\hat{A} = A_{22} - LA_{12}
```


```python
g = symbols('g', positive=True)
L = Matrix([[2 * g], [-Jmn * g**2]])
disp('L=', L)
Ahat = A22 - L @ A12
disp('\hat{A}=', Ahat)
# There is a residual imaginary number because of numerical error, therefore we
# show only the real part
disp(r'\text{Eigenvalues of }\hat{A}=', [re(k) for k in Ahat.eigenvals()])
```


$`\displaystyle L=\left[\begin{matrix}2 g\\- J_{mn} g^{2}\end{matrix}\right]`$



$`\displaystyle \hat{A}=\left[\begin{matrix}- 2.0 g & - \frac{1.0}{J_{mn}}\\1.0 J_{mn} g^{2} & 0\end{matrix}\right]`$



$`\displaystyle \text{Eigenvalues of }\hat{A}=\left[ - 1.0 g, \  - 1.0 g\right]`$


### Step 3

```math
k = \hat{A}L + A_{21} - LA_{11}
```
```math
D = S^{-1} \left[\begin{matrix}0\\I_{n-r}\end{matrix}\right]
```
```math
H = S^{-1} \left[\begin{matrix}I_r\\L\end{matrix}\right]
```
```math
MB = -LB_1 + B_2
```
```math
M = \left[\begin{matrix}-L&I_{n-r}\end{matrix}\right]S
```


```python
k = Ahat @ L + A21 - L @ A11
disp('k=', k)
D = S_1 @ np.vstack([np.zeros((r, n - r)), np.eye(n - r)])
disp('D=', D)
H = S_1 @ np.vstack([np.eye(r), L])
disp('H=', H)
MB = -L @ B1 + B2
disp('MB=', MB)
M = np.hstack([-L, np.eye(n - r)]) @ S
disp('M=', M)
```


$`\displaystyle k=\left[\begin{matrix}- 3.0 g^{2}\\2.0 J_{mn} g^{3}\end{matrix}\right]`$



$`\displaystyle D=\left[\begin{matrix}0 & 0\\1.0 & 0\\0 & 1.0\end{matrix}\right]`$



$`\displaystyle H=\left[\begin{matrix}1.0\\2.0 g\\- 1.0 J_{mn} g^{2}\end{matrix}\right]`$



$`\displaystyle MB=\left[\begin{matrix}\frac{K_{tn}}{J_{mn}}\\0\end{matrix}\right]`$



$`\displaystyle M=\left[\begin{matrix}- 2 g & 1.0 & 0\\J_{mn} g^{2} & 0 & 1.0\end{matrix}\right]`$


### Result

```math
\dot{z} = \hat{A}z + ky + MBu
```
```math
\hat{x} = Dz + Hy
```


```python
disp('\dot{z}=', Ahat, 'z + ', k, 'y + ', MB, 'u')
disp('\hat{x}=', D, 'z + ', H, 'y')
```


$`\displaystyle \dot{z}=\left[\begin{matrix}- 2.0 g & - \frac{1.0}{J_{mn}}\\1.0 J_{mn} g^{2} & 0\end{matrix}\right]z + \left[\begin{matrix}- 3.0 g^{2}\\2.0 J_{mn} g^{3}\end{matrix}\right]y + \left[\begin{matrix}\frac{K_{tn}}{J_{mn}}\\0\end{matrix}\right]u`$



$`\displaystyle \hat{x}=\left[\begin{matrix}0 & 0\\1.0 & 0\\0 & 1.0\end{matrix}\right]z + \left[\begin{matrix}1.0\\2.0 g\\- 1.0 J_{mn} g^{2}\end{matrix}\right]y`$


We check that our solution fullfills the required conditions to guarantee $`(e \rightarrow 0)`$:


```python
disp('\hat{A}M=', Ahat @ M, '= MA - kC =', M @ A - k @ C)
disp('I_n=', np.eye(n), '= DM + HC =', D @ M + H @ C)
disp(
    '\hat{A}: stable,\quad eigenvalues(\hat{A}) =',
    [k for k in Ahat.eigenvals()]
    )
```


$`\displaystyle \hat{A}M=\left[\begin{matrix}3.0 g^{2} & - 2.0 g & - \frac{1.0}{J_{mn}}\\- 2.0 J_{mn} g^{3} & 1.0 J_{mn} g^{2} & 0\end{matrix}\right]= MA - kC =\left[\begin{matrix}3.0 g^{2} & - 2 g & - \frac{1.0}{J_{mn}}\\- 2.0 J_{mn} g^{3} & J_{mn} g^{2} & 0\end{matrix}\right]`$



$`\displaystyle I_n=\left[\begin{matrix}1.0 & 0 & 0\\0 & 1.0 & 0\\0 & 0 & 1.0\end{matrix}\right]= DM + HC =\left[\begin{matrix}1.0 & 0 & 0\\0 & 1.0 & 0\\0 & 0 & 1.0\end{matrix}\right]`$



$`\displaystyle \hat{A}: stable,\quad eigenvalues(\hat{A}) =\left[ g \left(-1.0 + 2.87723372714458 \cdot 10^{-13} i\right), \  g \left(-1.0 - 2.70776203998741 \cdot 10^{-12} i\right)\right]`$


## Disturbance observer

From the minimal order observer obtained in the last section, we can obtain the
disturbance observer $`\hat{T}_{dis}`$


```python
z1, z2, dz1, dz2, s = symbols(r'z_1, z_2, \dot{z}_1, \dot{z}_2, s')
dz = Matrix([[dz1], [dz2]])
z = Matrix([[z1], [z2]])
disp('\hat{T}_{dis}=', D[2::2, :], z, ' + ', H[2, 0], r'\theta')
disp('\hat{T}_{dis}=', (D[2::2, :] @ z)[0], ' + ', H[2, 0], r'\theta')
```


$`\displaystyle \hat{T}_{dis}=\left[\begin{matrix}0 & 1.0\end{matrix}\right]\left[\begin{matrix}z_{1}\\z_{2}\end{matrix}\right] + - 1.0 J_{mn} g^{2}\theta`$



$`\displaystyle \hat{T}_{dis}=1.0 z_{2} + - 1.0 J_{mn} g^{2}\theta`$


In order to find a disturbance observer that depends on $`\theta`$ and
$`I_a^{ref}`$, we need to find a solution for $`z_2`$. We proceed to solve the
system of equations from the minimal order observer:


```python
th, Ia = y, u = symbols(r'\theta, I_a^{ref}')
disp(dz, '=', Ahat, z, ' + ', k, y, ' + ', MB, u)
disp(dz, '=', Ahat @ z + k * y + MB * u)
```


$`\displaystyle \left[\begin{matrix}\dot{z}_1\\\dot{z}_2\end{matrix}\right]=\left[\begin{matrix}- 2.0 g & - \frac{1.0}{J_{mn}}\\1.0 J_{mn} g^{2} & 0\end{matrix}\right]\left[\begin{matrix}z_{1}\\z_{2}\end{matrix}\right] + \left[\begin{matrix}- 3.0 g^{2}\\2.0 J_{mn} g^{3}\end{matrix}\right]\theta + \left[\begin{matrix}\frac{K_{tn}}{J_{mn}}\\0\end{matrix}\right]I_a^{ref}`$



$`\displaystyle \left[\begin{matrix}\dot{z}_1\\\dot{z}_2\end{matrix}\right]=\left[\begin{matrix}\frac{I_a^{ref} K_{tn}}{J_{mn}} - 3.0 \theta g^{2} - 2.0 g z_{1} - \frac{1.0 z_{2}}{J_{mn}}\\2.0 J_{mn} \theta g^{3} + 1.0 J_{mn} g^{2} z_{1}\end{matrix}\right]`$



```python
gdiff = symbols('g_{diff}', positive=True)
disp(s * z, '=', Ahat @ z + k * y + MB * u)
z2_sol = simplify(solve(s * z - (Ahat @ z + k * y + MB * u), z1, z2)[z2])
disp('z_2=', z2_sol)
```


$`\displaystyle \left[\begin{matrix}s z_{1}\\s z_{2}\end{matrix}\right]=\left[\begin{matrix}\frac{I_a^{ref} K_{tn}}{J_{mn}} - 3.0 \theta g^{2} - 2.0 g z_{1} - \frac{1.0 z_{2}}{J_{mn}}\\2.0 J_{mn} \theta g^{3} + 1.0 J_{mn} g^{2} z_{1}\end{matrix}\right]`$



$`\displaystyle z_2=\frac{g^{2} \left(I_a^{ref} K_{tn} + J_{mn} \theta g^{2} + 2.0 J_{mn} \theta g s\right)}{g^{2} + 2.0 g s + s^{2}}`$



```python
Tdis = z2_sol + H[2, 0] * y
disp(r'T_{dis}(I_a^{ref}, \theta)=', Tdis)
```


$`\displaystyle T_{dis}(I_a^{ref}, \theta)=- 1.0 J_{mn} \theta g^{2} + \frac{g^{2} \left(I_a^{ref} K_{tn} + J_{mn} \theta g^{2} + 2.0 J_{mn} \theta g s\right)}{g^{2} + 2.0 g s + s^{2}}`$


Which can be simplified into: 

```math
T_{dis}(I_a^{ref}, \theta)=
\left(\frac{g}{g + s}\right)^2
\left(J_{mn} g (g + 2s) \theta + K_{tn}I_a^{ref}\right)
- J_{mn} \theta g^{2}

```

# Using Pseudo differentiation

Pseudo differentiation can be defined as 
```math
\frac{\hat{\omega}}{\theta} = \frac{g_{diff}s}{s + g_{diff}}

```

Therefore we can compute $`\hat{T}_{dis}(I_a^{ref}, \theta)`$ from
$`\hat{T}_{dis}(I_a^{ref}, \omega)`$ given in the assignment statement by using
pseudo differentiation:

```math
\hat{T}_{dis}(I_a^{ref}, \omega) = \hat{T}_{dis}(I_a^{ref}, \hat{\omega}) = 
\hat{T}_{dis}(I_a^{ref}, \frac{g_{diff}s}{s + g_{diff}}\theta)

```

```math
T_{dis}(I_a^{ref}, \theta)= 
\frac{g}{g + s} 
\left( g^2 J_{mn} \frac{g_{diff}}{s + g_{diff}} \theta + g K_{tn} I_a^{ref}\right) 
- J_{mn} \frac{g_{diff}*s}{s + g_{diff}} \theta g

```

Which can be obtained too from the following block diagram:

![block diagram](images/block-diagram.png)
